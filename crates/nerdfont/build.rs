// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::{env, fs};
use std::path::Path;
use std::collections::{HashMap, HashSet};


// Build Script ---------------------------------------------------------------
// ----------------------------------------------------------------------------
fn main() {
    let cargo_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let font_path = Path::new(&cargo_dir).join("src").join("font");

    // Load font data
    let ttf_path = font_path.join("proggyclean_icons.ttf");
    let ttf = fs::read(&ttf_path).expect("Failed to load TTF file.");
    let full_glyph_ranges = parse_glyph_ranges(&ttf);

    // Parse icon names
    let icons_path = font_path.join("icons.json");
    let json = fs::read_to_string(&icons_path).expect("Failed to load icons from JSON file.");
    let icons: HashMap<String, u32> = serde_json::from_str(&json).expect("Failed to parse icons from JSON file.");
    let mut lines: Vec<String> = vec![
        format!("static FULL_GLYPH_RANGES: [u32; {}] = {:?};", full_glyph_ranges.len(), full_glyph_ranges),
        "pub mod icons {".to_string(),
        format!("pub const TOTAL_COUNT: usize = {};", icons.len())
    ];
    for (name, codepoint) in &icons {
        lines.push(format!("pub const {}: &str = \"\\u{{{:x}}}\";", name.replace('-', "_").to_uppercase(), codepoint));
    }
    lines.push("pub fn codepoint(icon: &str) -> u32 {".to_string());
    lines.push("match icon {".to_string());
    let mut unique = HashSet::new();
    for (name, codepoint) in &icons {
        if !unique.contains(&codepoint) {
            lines.push(format!("{} => {},", name.replace('-', "_").to_uppercase(), codepoint));
            unique.insert(codepoint);
        }
    }
    lines.push("_ => 0".to_string());
    lines.push("}".to_string());
    lines.push("}".to_string());
    lines.push("}".to_string());

    let out_dir = env::var("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("icons.rs");
    fs::write(&dest_path, &lines.join("\n").into_bytes()).expect("Failed to write output file");
}


// Parsing --------------------------------------------------------------------
// ----------------------------------------------------------------------------
fn parse_glyph_ranges(data: &[u8]) -> Vec<u32> {
    if let Ok(f) = ttf_parser::Face::from_slice(data, 0) {
        // Get all unique codepoints
        let mut codepoints = HashSet::with_capacity(f.number_of_glyphs().into());
        if let Some(ref table) = f.tables().cmap {
            for t in table.subtables {
                t.codepoints(|p| {
                    codepoints.insert(p);
                });
            }
        }

        // Sort
        let mut points: Vec<u32> = codepoints.into_iter().collect();
        points.sort_unstable();

        // Extract codepoint ranges
        let mut ranges = Vec::with_capacity(8);
        let mut points = points.into_iter();
        if let Some(mut last) = points.next() {

            // Can't start with a zero
            let mut first = last.max(1);
            for next in points {
                if next.saturating_sub(last) != 1 {
                    ranges.push(first);
                    ranges.push(last);
                    first = next;
                }
                last = next;
            }
            ranges.push(first);
            ranges.push(last);
        }

        // Must end with a 0 "marker"
        ranges.push(0);
        ranges

    } else {
        vec![0]
    }
}

