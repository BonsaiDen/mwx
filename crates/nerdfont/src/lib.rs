// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::HashSet;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use imgui::FontGlyphRanges;


// Constants ------------------------------------------------------------------
// ----------------------------------------------------------------------------
const ICON_FONT_DATA: &[u8] = include_bytes!("./font/proggyclean_icons.ttf");


// Functions ------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub fn font_data() -> &'static [u8] {
    ICON_FONT_DATA
}

pub fn font_glyph_ranges(icons: & [&str]) -> FontGlyphRanges {
    if icons.is_empty() {
        imgui::FontGlyphRanges::from_slice(&FULL_GLYPH_RANGES[0..])

    } else {
        let unique: HashSet<u32> = icons.iter().map(|i| crate::icons::codepoint(i)).collect();
        let mut points: Vec<u32> = unique.into_iter().collect();
        points.sort_unstable();

        // Extract codepoint ranges
        let mut ranges = vec![1, 255];
        let mut points = points.into_iter();
        if let Some(mut last) = points.next() {
            // Can't start with a zero
            let mut first = last.max(1);
            for next in points {
                if next.saturating_sub(last) != 1 {
                    ranges.push(first);
                    ranges.push(last);
                    first = next;
                }
                last = next;
            }
            ranges.push(first);
            ranges.push(last);
        }

        // Must end with a 0 "marker"
        ranges.push(0);

        // Create the range, forgetting the vector to ensure the data is kept
        // around until the end of the program
        let ptr = ranges.as_ptr();
        std::mem::forget(ranges);
        unsafe { imgui::FontGlyphRanges::from_ptr(ptr) }
    }
}


// Definitions ----------------------------------------------------------------
// ----------------------------------------------------------------------------
include!(concat!(env!("OUT_DIR"), "/icons.rs"));

