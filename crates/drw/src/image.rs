// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use glam::{Vec2, UVec2};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{Color, PixelSampler};
use crate::fast::{read_argb32_no_bounds, f32_to_usize};


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait PxlImageBuffer {
    fn pxl_image<S: Into<UVec2>>(self, size: S) -> PxlImage where Self: Sized;
}

impl PxlImageBuffer for &[u8] {
    fn pxl_image<S: Into<UVec2>>(self, size: S) -> PxlImage {
        PxlImage::from_rgba8_buffer(self, size)
    }
}

impl PxlImageBuffer for Vec<u8> {
    fn pxl_image<S: Into<UVec2>>(self, size: S) -> PxlImage {
        PxlImage::from_rgba8_buffer(&self, size)
    }
}

impl PxlImageBuffer for &[u32] {
    fn pxl_image<S: Into<UVec2>>(self, size: S) -> PxlImage {
        PxlImage::from_argb32_buffer(self.to_vec(), size)
    }
}

impl PxlImageBuffer for Vec<u32> {
    fn pxl_image<S: Into<UVec2>>(self, size: S) -> PxlImage {
        PxlImage::from_argb32_buffer(self, size)
    }
}


// Image ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct PxlImage {
    buffer: Vec<u32>,
    pixel_width: usize,
    width: f32,
    height: f32
}

impl PxlImage {
    pub fn from_argb32_buffer<S: Into<UVec2>>(buffer: Vec<u32>, size: S) -> Self {
        let size = size.into();
        assert!(buffer.len() == (size.x * size.y) as usize, "invalid pixel buffer size");
        Self {
            buffer,
            pixel_width: size.x as usize,
            width: size.x as f32,
            height: size.y as f32
        }
    }

    pub fn from_rgba8_buffer<S: Into<UVec2>>(buffer: &[u8], size: S) -> Self {
        let size = size.into();
        assert!(buffer.len() == (size.x * size.y * 4) as usize, "invalid pixel buffer size");

        let mut pixels: Vec<u32> = Vec::with_capacity((size.x * size.y) as usize);
        for i in 0..buffer.len() / 4 {
            let r = (buffer[i * 4] as u32) << 16;
            let g = (buffer[i * 4 + 1] as u32) << 8;
            let b = buffer[i * 4 + 2] as u32;
            let a = (buffer[i * 4 + 3] as u32) << 24;
            pixels.push(a | r | g | b);
        }
        Self::from_argb32_buffer(pixels, size)
    }

    pub fn size(&self) -> Vec2 {
        Vec2::new(self.width, self.height)
    }
}

impl PixelSampler for PxlImage {
    fn sample_pixel(&self, _: u32, _: u32, u: f32, v: f32, _: Color) -> Color {
        let px = f32_to_usize(u * self.width);
        let py = f32_to_usize(v * self.height);

        // Safety: u and v are clamped to 0..1 by the Rasterizer
        read_argb32_no_bounds(&self.buffer, px + py * self.pixel_width)
    }
}

