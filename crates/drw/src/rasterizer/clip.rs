// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::mem;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use glam::Vec2;
use smallvec::{SmallVec, smallvec};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::Vec2UV;


// Rect Clipping --------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Clip;
impl Clip {
    pub fn line(mut a: Vec2, mut b: Vec2, clipping: &[Vec2]) -> Option<(Vec2, Vec2)> {
        for i in 0..clipping.len() {
            let k = (i + 1) % clipping.len();
            if !Self::line_axis(clipping[i], clipping[k], &mut a, &mut b) {
                return None;
            }
        }
        Some((a, b))
    }

    fn line_axis(a: Vec2, b: Vec2, i: &mut Vec2, k: &mut Vec2) -> bool {
        let ip = (b.x - a.x) * (i.y - a.y) - (b.y - a.y) * (i.x - a.x);
        let kp = (b.x - a.x) * (k.y - a.y) - (b.y - a.y) * (k.x - a.x);

        // Case 1: When both points are inside
        if ip > 0.0 && kp > 0.0 {
            true

        // Case 2: When only first point is outside
        } else if ip <= 0.0 && kp > 0.0 {
            let t = Self::intersection_point(a, b, *i, *k);
            *i = i.lerp(*k, t);
            true

        // Case 3: When only second point is outside
        } else if ip > 0.0 && kp <= 0.0 {
            let t = Self::intersection_point(a, b, *i, *k);
            *k = i.lerp(*k, t);
            true

        } else {
            false
        }
    }

    pub fn polygon<C: FnMut(Vec2UV, Vec2UV, Vec2UV)>(
        a: Vec2UV,
        b: Vec2UV,
        c: Vec2UV,
        clipping: &[Vec2],
        mut callback: C
    ) {
        let mut tmp: SmallVec<[_; 8]> = smallvec![];
        let mut output: SmallVec<[_; 8]> = smallvec![a, b, c];

        let handedness = a.triangle_area_times_two(c, b).signum();
        for i in 0..clipping.len() {
            let k = (i + 1) % clipping.len();
            Self::poly_axis(&output, &mut tmp, clipping[i], clipping[k], handedness);
            mem::swap(&mut output, &mut tmp);
            tmp.clear();
        }

        if !output.is_empty() {
            // Safety: The algorithm guarantees that the output is either empty or contains at
            // least 3 vertices.
            //
            // Replace with array_windows<N> once stable
            let initial = unsafe { *output.get_unchecked(0) };
            for i in 1..output.len() - 1 {
                callback(
                    initial,
                    unsafe { *output.get_unchecked(i) },
                    unsafe { *output.get_unchecked(i + 1) }
                );
            }
        }
    }

    fn poly_axis(input: &[Vec2UV], output: &mut SmallVec<[Vec2UV; 8]>, a: Vec2, b: Vec2, handedness: f32) {
        for i in 0..input.len() {
            let k = (i + 1) % input.len();
            let i = input[i];
            let k = input[k];

            let ip = ((b.x - a.x) * (i.pos.y - a.y) - (b.y - a.y) * (i.pos.x - a.x)) * handedness;
            let kp = ((b.x - a.x) * (k.pos.y - a.y) - (b.y - a.y) * (k.pos.x - a.x)) * handedness;

            // Case 1: When both points are inside
            if ip < 0.0 && kp < 0.0 {
                output.push(k);

            // Case 2: When only first point is outside
            } else if ip >= 0.0 && kp < 0.0 {
                let t = Self::intersection_point(a, b, i.pos, k.pos);
                output.push(i.lerp(k, t));
                output.push(k);

            // Case 3: When only second point is outside
            } else if ip < 0.0 && kp >= 0.0 {
                let t = Self::intersection_point(a, b, i.pos, k.pos);
                output.push(i.lerp(k, t));
            }
        }
    }

    fn intersection_point(a: Vec2, b: Vec2, c: Vec2, d: Vec2) -> f32 {
        let det = (d.y - c.y) * (b.x - a.x) - (d.x - c.x) * (b.y - a.y);
        ((b.x - a.x) * (a.y - c.y) - (b.y - a.y) * (a.x - c.x)) / det
    }
}

