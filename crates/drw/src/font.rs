// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::cell::RefCell;
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use rusttype::{point, Font as RustFont, Scale};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{Align, Color, PixelSampler};
use crate::fast::{read_pixel_no_bounds, f32_to_usize};


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Debug, Hash, Eq, PartialEq)]
struct GlyphIndex(char, i32);
impl GlyphIndex {
    fn new(c: char, pt: f32) -> Self {
        Self(c, (pt * 1000.0) as i32)
    }
}

#[derive(Clone)]
struct RasterizedGlyph {
    x: i32,
    y: i32,
    advance: i32,
    pixel_width: usize,
    pixel_height: usize,
    pixels: Option<Vec<u8>>
}

pub struct SamplerGlyph<'a> {
    width: f32,
    height: f32,
    pixel_width: usize,
    pixels: &'a [u8]
}

impl<'a> PixelSampler for SamplerGlyph<'a> {
    fn sample_pixel(&self, _: u32, _: u32, u: f32, v: f32, color: Color) -> Color {
        let px = f32_to_usize(u * self.width);
        let py = f32_to_usize(v * self.height);
        let i = px + py * self.pixel_width;

        // Safety: u and v are clamped to 0..1 by the Rasterizer
        let a = read_pixel_no_bounds(self.pixels, i);

        // Multiply color alpha with glyph pixel alpha
        color.with_alpha(((color.a() as u16 * a as u16 ) / 255) as u8)
    }
}


// Font Implementation --------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct PxlFont {
    font: RustFont<'static>,
    glyphs: RefCell<HashMap<GlyphIndex, RasterizedGlyph>>
}

impl PxlFont {
    pub fn try_from_bytes(bytes: &'static [u8]) -> Result<Self, String> {
        Ok(Self {
            font: RustFont::try_from_bytes(bytes).expect("failed to parse font"),
            glyphs: RefCell::new(HashMap::with_capacity(256))
        })
    }

    pub(crate) fn layout<C: FnMut(i32, i32, usize, usize, SamplerGlyph)>(
        &self,
        text: &str,
        pt: f32,
        align: (Align, Align),
        mut callback: C
    ) {
        // Calculate size and pre-rasterize any new glyphs
        let line_height = pt.round() as i32;
        let mut x = 0;
        let mut width = 0;
        let mut height = line_height;
        for c in text.chars() {
            if c == '\n' || c == '\r' {
                x = 0;
                height += line_height;

            } else {
                self.rasterize_glyph(c, pt);
                if let Some(glyph) = self.glyphs.borrow().get(&GlyphIndex::new(c, pt)) {
                    x += glyph.advance;
                }
            }
            width = width.max(x);
        }

        // Layout glyph coordinates
        let mut x = 0;
        let mut y = 0;
        for c in text.chars() {
            if c == '\n' || c == '\r' {
                x = 0;
                y += line_height;

            } else if let Some(glyph) = self.glyphs.borrow().get(&GlyphIndex::new(c, pt)) {
                if let Some(pixels) = &glyph.pixels {
                    callback(
                        glyph.x + align.0.apply_text(x, width),
                        glyph.y + align.1.apply_text(y, height),
                        glyph.pixel_width,
                        glyph.pixel_height,
                        SamplerGlyph {
                            width: glyph.pixel_width as f32,
                            height: glyph.pixel_height as f32,
                            pixel_width: glyph.pixel_width,
                            pixels
                        }
                    );
                }
                x += glyph.advance;
            }
        }
    }

    fn rasterize_glyph(&self, c: char, pt: f32) {
        let index = GlyphIndex::new(c, pt);
        if self.glyphs.borrow().contains_key(&index) {
            return;

        } else if c == ' ' {
            self.rasterize_space(pt);
            return;
        }

        let s = format!("{}", c);
        let scale = Scale {
            x: pt,
            y: pt
        };
        let mut glyphs = self.font.layout(&s, scale, point(0.0, 0.0));
        let mut glyph = None;
        if let Some(g) = glyphs.next() {
            if let Some(bb) = g.pixel_bounding_box() {
                let v_metrics = self.font.v_metrics(scale);
                let pixel_width = (bb.max.x - bb.min.x) as usize;
                let pixel_height = (bb.max.y - bb.min.y) as usize;

                let mut pixels: Vec<u8> = Vec::with_capacity(pixel_width * pixel_height);
                g.draw(|_, _, v| {
                    pixels.push(if v > 0.5 {
                        (v * 255.0) as u8

                    } else {
                        0
                    });
                });
                glyph = Some(RasterizedGlyph {
                    x: bb.min.x as i32,
                    y: bb.min.y as i32 + v_metrics.ascent as i32,
                    advance: g.unpositioned().h_metrics().advance_width as i32,
                    pixel_width,
                    pixel_height,
                    pixels: Some(pixels)
                });
            }
        }

        // Always insert a empty fallback to avoid repeated rasterization of missing glyphs
        let glyph = glyph.unwrap_or_else(|| {
            self.rasterize_space(pt);
            self.glyphs.borrow().get(&GlyphIndex::new(' ', pt)).unwrap().clone()
        });
        self.glyphs.borrow_mut().insert(index, glyph);
    }

    fn rasterize_space(&self, pt: f32) {
        let index = GlyphIndex::new(' ', pt);
        if self.glyphs.borrow().contains_key(&index) {
            return;
        }

        let scale = Scale {
            x: pt,
            y: pt
        };
        let mut glyphs = self.font.layout(" I", scale, point(0.0, 0.0));
        let mut space_width = 8;
        if let Some(g) = glyphs.nth(1) {
            if let Some(bb) = g.pixel_bounding_box() {
                space_width = bb.min.x as i32;
            }
        }
        self.glyphs.borrow_mut().insert(index, RasterizedGlyph {
            x: 0,
            y: 0,
            advance: space_width,
            pixel_width: space_width as usize,
            pixel_height: pt.round() as usize,
            pixels: None
        });
    }
}

