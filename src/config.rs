// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;
use std::path::PathBuf;
use std::collections::HashMap;
use std::time::{Duration, Instant};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use serde::{Serialize, Deserialize, de::DeserializeOwned};


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait Config: Default + std::fmt::Debug + Clone + PartialEq + Eq + Serialize + DeserializeOwned {
    fn filename() -> Option<PathBuf> {
        Some(".app.toml".into())
    }
}


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Window {
    #[serde(default)]
    pub position: Option<(i32, i32)>,
    #[serde(default)]
    pub size: Option<(u32, u32)>
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct App<T: Config> {
    pub windows: HashMap<String, Window>,
    #[serde(bound(deserialize = ""))]
    pub inner: T,
}

impl<T: Config> App<T> {
    pub fn load_from_toml() -> Result<Self, Box<dyn Error>> {
        match T::filename() {
            Some(filename) => {
                let t = std::fs::read_to_string(filename)?;
                let s = toml::from_str::<Self>(&t)?;
                Ok(s)
            },
            None => Ok(Self::default())
        }
    }

    pub fn compare_and_save(&mut self, saved: &mut Self, inner: &T, timer: &mut Instant, immediately: bool) {
        if (timer.elapsed() > Duration::from_millis(500) || immediately) && (self.windows != saved.windows || &self.inner != inner)  {
            self.inner = inner.clone();
            if let Some(filename) = T::filename() {
                match toml::to_string(&self)  {
                    Ok(t) => {
                        log::info!("Serialized to {:?}", filename);
                        std::fs::write(filename, t).ok();
                    },
                    Err(err) => {
                        log::error!("Failed serialization to {:?}: {}", filename, err);
                        log::debug!("Configuration:\n{:#?}", self);
                    }
                }
            }
            *saved = self.clone();
            *timer = Instant::now();
        }
    }
}

