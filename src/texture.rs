// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{ctx, handle};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub(crate) struct TextureData {
    pub id: imgui::TextureId,
    pub bytes: Vec<u8>
}

pub struct Texture {
    width: u32,
    height: u32,
    fill_color: [u8; 4],
    data: Option<TextureData>
}

impl Texture {
    pub fn new(width: u32, height: u32, fill_color: [u8; 4]) -> Self {
        Self {
            width,
            height,
            fill_color,
            data: None
        }
    }

    pub fn id(&mut self, ctx: &ctx::UI) -> imgui::TextureId {
        self.ensure_init(ctx);
        if let Some(TextureData { id, .. }) = &self.data {
            *id

        } else {
            unreachable!("texture initialization missing");
        }
    }

    pub fn handle<'a, 'b>(&'a mut self, ctx: &'a ctx::UI<'b>) -> handle::Texture<'a, 'b> {
        self.ensure_init(ctx);
        if let Some(data) = &mut self.data {
            handle::Texture {
                ctx,
                queued: false,
                width: self.width as u32,
                height: self.height as u32,
                data
            }

        } else {
            unreachable!("texture initialization missing");
        }
    }

    fn ensure_init(&mut self, ctx: &ctx::UI) {
        if self.data.is_none() {
            let width = self.width;
            let height = self.height;
            let mut data: Vec<u8> = std::iter::repeat(0).take(width as usize * height as usize * 4).collect();
            for d in data.chunks_exact_mut(4) {
                d.copy_from_slice(&self.fill_color);
            }
            self.data = Some(ctx.create_texture_data(width, height, data));
        }
    }
}

