// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use spin_sleep::LoopHelper;
use winit::event_loop::{ControlFlow, EventLoop};
use winit::platform::run_return::EventLoopExtRunReturn;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::window::WindowInstance;
use crate::{ctx, config, event, handle, AppInterface, Output};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
enum FPSLimiter {
    Limit(LoopHelper),
    ReportOnly(LoopHelper)
}

impl FPSLimiter {
    fn from_frame_rate(frame_rate: Option<u8>) -> Self {
        match frame_rate {
            Some(f) => FPSLimiter::Limit(
                LoopHelper::builder().report_interval_s(0.1).build_with_target_rate(f)
            ),
            None => FPSLimiter::ReportOnly(
                LoopHelper::builder().report_interval_s(0.1).build_with_target_rate(60)
            ),
        }
    }

    fn limit<T: AppInterface>(&mut self, handle: &mut handle::App<T>) {
        match self {
            FPSLimiter::Limit(ref mut limiter) => {
                limiter.loop_sleep();
                handle.dt = limiter.loop_start();
                if let Some(f) = limiter.report_rate() {
                    handle.fps = f as f32;
                }
            },
            FPSLimiter::ReportOnly(ref mut limiter) => {
                handle.dt = limiter.loop_start();
                if let Some(f) = limiter.report_rate() {
                    handle.fps = f as f32;
                }
            }
        }
    }
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub fn run<T: 'static + AppInterface>(mut app: T, frame_rate: Option<u8>) -> Output<T::Output> {

    // Initialize
    let start = std::time::Instant::now();
    let mut handle = handle::App::<T>::from_app(&mut app);
    let mut fps = FPSLimiter::from_frame_rate(frame_rate);
    let mut audio = ctx::Audio::new(8192);
    let mut event_loop = EventLoop::new();
    let mut windows = HashMap::new();
    let mut ctx = ctx::App::new(&mut handle, &mut windows, &event_loop);
    log::info!("Set up in {:?}", start.elapsed());

    app.event(&mut ctx, &mut audio, event::App::Init);
    log::info!("Initialized in {:?}", start.elapsed());

    // Run Event Loop
    event_loop.run_return(|event, window_target, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            winit::event::Event::RedrawRequested(window_id) => if let Some(Some(window)) = windows.get_mut(&window_id) {
                window.redraw(app.state_mut(), &mut handle);
            },
            winit::event::Event::MainEventsCleared => {
                // Forward State Events
                while let Some(e) = handle.state_events.pop_front() {
                    handle.events.push_back(event::App::State(e));
                }

                // Remove any closed windows
                windows.retain(|_, window| {
                    if window.as_ref().map(|w| w.handle().should_close()).unwrap_or(false) {
                        if let Some(window) = window.take() {
                            window.close(app.state_mut(), &mut handle);
                        }
                        false

                    } else {
                        true
                    }
                });

                // Run Window Logic
                let mut sorted_windows: Vec<&mut WindowInstance<T>> = windows.values_mut().flatten().collect();
                sorted_windows.sort_by_key(|a| a.priority());

                let mut actions = Vec::new();
                for window in sorted_windows {
                    if window.handle().initialize() {
                        window.init(app.state_mut(), &mut handle);
                    }
                    if !window.handle().should_close() {
                        if let Some(action) = window.action() {
                            actions.push(action);
                        }
                        window.update(app.state_mut(), &mut handle);
                        if !handle.config.windows.contains_key(window.name()) {
                            handle.config.windows.insert(window.name().to_string(), config::Window::default());
                        }
                        if let Some(state) = handle.config.windows.get_mut(window.name()) {
                            state.position = Some(window.position());
                            state.size = Some(window.size());
                        }
                    }
                }

                // Run Application Logic
                let mut ctx = ctx::App::new(&mut handle, &mut windows, window_target);
                while let Some(e) = ctx.pop_event() {
                    app.event(&mut ctx, &mut audio, e);
                }
                for action in actions {
                    app.event(&mut ctx, &mut audio, event::App::Action(action));
                }
                app.event(&mut ctx, &mut audio, event::App::Tick);
                if handle.should_exit {
                    *control_flow = ControlFlow::Exit;
                    return;
                }

                // Serialize config on change
                handle.compare_and_save_config(&mut app, false);

                // Limit frame rate
                fps.limit(&mut handle);
            },
            ref e @ winit::event::Event::WindowEvent { ref event, window_id } => {
                if let Some(Some(window)) = windows.get_mut(&window_id) {
                    if let winit::event::WindowEvent::CloseRequested | winit::event::WindowEvent::Destroyed = event {
                        window.handle().close_mut();
                    }
                    window.window_event(app.state_mut(), &mut handle, e);
                }
            },
            e => for window in windows.values_mut().flatten() {
                window.event(&e);
            }
        }
    });

    // Serialize Config before exit
    handle.compare_and_save_config(&mut app, true);

    // Run exit handler
    let mut ctx = ctx::App::new(&mut handle, &mut windows, &event_loop);
    let result = app.closed(&mut ctx);
    log::info!("Closed after {:?}", start.elapsed());
    result
}

