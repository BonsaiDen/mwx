// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use winit::window::WindowId;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{ctx, AppInterface, ContextMenu, State};


// Enums ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub enum App<T: AppInterface> {
    Init,
    Tick,
    Action(<T::ContextMenu as ContextMenu<T::State>>::Action),
    State(<T::State as State>::Event),
    WindowClosed(WindowId)
}

pub enum Window<'a> {
    Init,
    Tick,
    RenderPixels(&'a mut ctx::Pixels),
    RenderUI(&'a ctx::UI<'a>),
    DroppedFile(PathBuf)
}

