// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
pub use winit::event::VirtualKeyCode as Key;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait KeyName {
    fn as_name(&self) -> &str;
}

impl KeyName for Key {
    fn as_name(&self) -> &str {
        match self {
            Key::Key1 => "1",
            Key::Key2 => "2",
            Key::Key3 => "3",
            Key::Key4 => "4",
            Key::Key5 => "5",
            Key::Key6 => "6",
            Key::Key7 => "7",
            Key::Key8 => "8",
            Key::Key9 => "9",
            Key::Key0 => "0",

            Key::A => "A",
            Key::B => "B",
            Key::C => "C",
            Key::D => "D",
            Key::E => "E",
            Key::F => "F",
            Key::G => "G",
            Key::H => "H",
            Key::I => "I",
            Key::J => "J",
            Key::K => "K",
            Key::L => "L",
            Key::M => "M",
            Key::N => "N",
            Key::O => "O",
            Key::P => "P",
            Key::Q => "Q",
            Key::R => "R",
            Key::S => "S",
            Key::T => "T",
            Key::U => "U",
            Key::V => "V",
            Key::W => "W",
            Key::X => "X",
            Key::Y => "Y",
            Key::Z => "Z",

            Key::Escape => "ESC",

            Key::F1 => "F1",
            Key::F2 => "F2",
            Key::F3 => "F3",
            Key::F4 => "F4",
            Key::F5 => "F5",
            Key::F6 => "F6",
            Key::F7 => "F7",
            Key::F8 => "F8",
            Key::F9 => "F9",
            Key::F10 => "F10",
            Key::F11 => "F11",
            Key::F12 => "F12",
            Key::F13 => "F13",
            Key::F14 => "F14",
            Key::F15 => "F15",
            Key::F16 => "F16",
            Key::F17 => "F17",
            Key::F18 => "F18",
            Key::F19 => "F19",
            Key::F20 => "F20",
            Key::F21 => "F21",
            Key::F22 => "F22",
            Key::F23 => "F23",
            Key::F24 => "F24",

            Key::Snapshot => "Print",
            Key::Scroll => "Scroll",
            Key::Pause => "Pause",

            Key::Insert => "Insert",
            Key::Home => "Home",
            Key::Delete => "Delete",
            Key::End => "End",
            Key::PageDown => "Page Down",
            Key::PageUp => "Page Up",

            Key::Left => "Left",
            Key::Up => "Up",
            Key::Right => "Right",
            Key::Down => "Down",

            Key::Back => "Backspace",
            Key::Return => "Enter",
            Key::Space => "Space",

            Key::Tab => "Tab",

            _ => "Unkown"
        }
    }
}

