// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;
use std::path::PathBuf;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use nfde::{
    Nfd,
    DialogResult,
    OpenFileDialogBuilder, OpenFileMultipleDialogBuilder,
    PickFolderDialogBuilder,
    SaveFileDialogBuilder,
    SingleFileDialogBuilder, MultipleFileDialogBuilder
};


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait FileDialog {
    fn open_file<C: FnMut(&mut OpenFileDialogBuilder) -> Result<&mut OpenFileDialogBuilder, nfde::Error>>(&self, mut callback: C) -> Result<Option<PathBuf>, Box<dyn Error>> {
        let mut nfd = Nfd::new()?.open_file();
        callback(&mut nfd)?;
        match nfd.show() {
            DialogResult::Ok(p) => Ok(Some(p.to_path_buf())),
            DialogResult::Cancel => Ok(None),
            DialogResult::Err(err) => Err(err.into())
        }
    }

    fn open_file_multiple<C: FnMut(&mut OpenFileMultipleDialogBuilder) -> Result<&mut OpenFileMultipleDialogBuilder, nfde::Error>>(&self, mut callback: C) -> Result<Option<Vec<PathBuf>>, Box<dyn Error>> {
        let mut nfd = Nfd::new()?.open_file_multiple();
        callback(&mut nfd)?;
        match nfd.show() {
            DialogResult::Ok(p) => {
                let paths = p.iter().flat_map(|p| p.ok().map(|p| p.to_path_buf())).collect();
                Ok(Some(paths))
            }
            DialogResult::Cancel => Ok(None),
            DialogResult::Err(err) => Err(err.into())
        }
    }

    fn pick_folder<C: FnMut(&mut PickFolderDialogBuilder) -> Result<&mut PickFolderDialogBuilder, nfde::Error>>(&self, mut callback: C)  -> Result<Option<PathBuf>, Box<dyn Error>> {
        let mut nfd = Nfd::new()?.pick_folder();
        callback(&mut nfd)?;
        match nfd.show() {
            DialogResult::Ok(p) => Ok(Some(p.to_path_buf())),
            DialogResult::Cancel => Ok(None),
            DialogResult::Err(err) => Err(err.into())
        }
    }

    fn save_file<C: FnMut(&mut SaveFileDialogBuilder) -> Result<&mut SaveFileDialogBuilder, nfde::Error>>(&self, mut callback: C) -> Result<Option<PathBuf>, Box<dyn Error>> {
        let mut nfd = Nfd::new()?.save_file();
        callback(&mut nfd)?;
        match nfd.show() {
            DialogResult::Ok(p) => Ok(Some(p.to_path_buf())),
            DialogResult::Cancel => Ok(None),
            DialogResult::Err(err) => Err(err.into())
        }
    }
}

impl FileDialog for imgui::Ui<'_> {}

