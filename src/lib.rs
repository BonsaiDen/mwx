// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod app;
mod ctx;
mod config;
mod file_dialog;
mod key;
mod renderer;
mod texture;
mod window;


// Exports --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use ctx::*;
pub use config::Config;
pub use file_dialog::*;
pub use key::*;
pub use texture::Texture;
pub mod event;
pub mod handle;


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use drw;
pub use imgui;
pub use implot;
pub use winit;
pub use nerdfont::icons;
pub use nfde;
pub use const_format::concatcp as icons_concat;


// Macros ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[macro_export]
macro_rules! icon_str {
    (@convert $icon:ident, $($tail:tt)*) => {
        $crate::icons::$icon
    };
    (@convert $text:literal, $($tail:tt)*) => {
        $text
    };
    ($($tail:tt)*) => {
        $crate::icons_concat!($(icon_str!(@convert $tail,)),*)
    };
}

#[macro_export]
macro_rules! icon_list {
    ($($icon:ident),*) => {
        &[$($crate::icons::$icon),*]
    };
}


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type Output<T> = std::result::Result<T, Box<dyn Error>>;


// Traits ---------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub trait State {
    type Event;
    type Config: Config;

    fn config_mut(&mut self) -> &mut Self::Config;
}

pub trait ContextMenu<T: State> {
    type Action;

    fn render_ui(&self, ctx: &mut ctx::Window<T>, ui: &ctx::UI) -> Option<Self::Action>;
}

pub trait AppInterface: Sized {
    type Output;
    type State: State;
    type ContextMenu: ContextMenu<Self::State>;

    fn state_mut(&mut self) -> &mut Self::State;

    fn event(&mut self, ctx: &mut ctx::App<Self>, audio: &mut ctx::Audio, event: event::App<Self>);
    fn closed(self, ctx: &mut ctx::App<Self>) -> Output<Self::Output> where Self: Sized;

    fn run(self, frame_rate: Option<u8>) -> Output<Self::Output> where Self: 'static + Sized {
        crate::app::run(self, frame_rate)
    }
}

pub trait WindowInterface<T: AppInterface> {
    fn event(&mut self, _ctx: &mut ctx::Window<T::State>, _event: event::Window) {}
    fn closed(self, _ctx: &mut ctx::Window<T::State>) where Self: Sized {}
}

