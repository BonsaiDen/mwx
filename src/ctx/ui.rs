// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::ops::Deref;
use std::cell::RefCell;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::renderer::Renderer;
use crate::texture::TextureData;
use crate::window::ContextMenuCallback;
use crate::{ctx, ContextMenu, State};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct UI<'a> {
    pub(crate) imgui: &'a imgui::Ui<'a>,
    pub(crate) renderer: Rc<RefCell<Renderer>>,
    pub(crate) pixels: &'a mut ctx::Pixels,
    pub(crate) pixels_screen_layout: (f32, f32, f32),
    pub(crate) context_menu: ContextMenuCallback,
    pub(crate) has_context_menu: bool
}

impl<'a> Deref for UI<'a> {
    type Target = imgui::Ui<'a>;
    fn deref(&self) -> &Self::Target {
        self.imgui
    }
}

impl<'a> UI<'a> {
    pub fn has_context_menu_open(&self) -> bool {
        self.has_context_menu
    }

    pub fn open_context_menu<T: 'static + ContextMenu<S>, S: State>(&self, menu: T) {
        *self.context_menu.borrow_mut() = Some(Box::new(menu));
    }

    pub fn text_tooltip(&self, text: &str) {
        if !self.has_context_menu_open() {
            self.imgui.tooltip(|| self.imgui.text(text));
        }
    }

    pub fn pixel_scale(&self) -> f32 {
        self.pixels_screen_layout.2
    }

    pub fn pixel_offset_rect(&self, pos: [f32; 2], size: [f32; 2]) -> ([f32; 2], [f32; 2]) {
        let d = self.imgui.io().display_size;
        let tl = [
            (self.pixels_screen_layout.0 + pos[0] * self.pixels_screen_layout.2).max(0.0),
            (self.pixels_screen_layout.1 + pos[1] * self.pixels_screen_layout.2).max(0.0)
        ];
        let br = [
            (self.pixels_screen_layout.0 + size[0] * self.pixels_screen_layout.2).min(d[0]),
            (self.pixels_screen_layout.1 + size[1] * self.pixels_screen_layout.2).min(d[1])
        ];
        (tl, br)
    }

    pub fn pixel_offset_position(&self, pos: [f32; 2], size: [f32; 2]) -> ([f32; 2], [f32; 2]) {
        ([
            self.pixels_screen_layout.0 + pos[0] * self.pixels_screen_layout.2,
            self.pixels_screen_layout.1 + pos[1] * self.pixels_screen_layout.2

        ], [
            size[0] * self.pixels_screen_layout.2,
            size[1] * self.pixels_screen_layout.2
        ])
    }
}

impl<'a> UI<'a> {
    pub(crate) fn update_texture_data(&self, data: &TextureData) {
        self.renderer.borrow().update_texture_data(self.pixels.inner.context(), data);
    }

    pub(crate) fn create_texture_data(&self, width: u32, height: u32, bytes: Vec<u8>) -> TextureData {
        self.renderer.borrow_mut().create_texture_data(self.pixels.inner.context(), width, height, bytes)
    }
}

