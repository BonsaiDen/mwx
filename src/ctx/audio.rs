// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;
use std::sync::{Arc, Mutex};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crossbeam::queue::ArrayQueue;
use cpal::{Sample, SampleFormat, SampleRate, Stream};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub struct Audio {
    queue: Arc<ArrayQueue<f32>>,
    silent: Rc<RefCell<bool>>,
    stream: Arc<Mutex<Option<Stream>>>
}

impl Audio {
    pub fn initialize(&mut self, sample_rate: u32, channels: u16) -> Result<(), Box<dyn std::error::Error>> {
        // Create configuration
        let device = cpal::default_host().default_output_device().ok_or_else(|| "No audio device available".to_string())?;
        let config = device.supported_output_configs()?.find(|c| {
            c.channels() == channels && c.sample_format() == SampleFormat::F32

        }).ok_or_else(|| {
            "No audio device configuration available".to_string()

        })?.with_sample_rate(SampleRate(sample_rate)).into();

        // Create stream and start playback
        let mut last_value = 0.0;
        let handle = self.queue.clone();
        let stream = device.build_output_stream(&config, move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
            Audio::write(&handle, &mut last_value, data)

        }, |err| eprintln!("an error occurred on the output audio stream: {}", err))?;
        stream.play()?;

        if let Ok(mut s) = self.stream.lock() {
            *s = Some(stream);
        }
        Ok(())
    }

    pub fn push_sample(&self, left: f32, right: f32) {
        if !*self.silent.borrow() {
            self.queue.push(left).ok();
            self.queue.push(right).ok();
        }
    }

    pub fn set_silent(&self, silent: bool) {
        while self.queue.pop().is_some() {}
        if silent {
            self.queue.push(0.0).ok();
        }
        *self.silent.borrow_mut() = silent;
    }

    fn write<T: Sample>(queue: &Arc<ArrayQueue<f32>>, last_value: &mut f32, output: &mut [T]) {
        for sample in output.iter_mut() {
            *last_value = queue.pop().unwrap_or(*last_value);
            *sample = Sample::from(last_value);
        }
    }
}

impl Audio {
    pub(crate) fn new(buffer_size: usize) -> Self {
        Self {
            queue: Arc::new(ArrayQueue::<f32>::new(buffer_size)),
            silent: Rc::new(RefCell::new(false)),
            stream: Arc::new(Mutex::new(None))
        }
    }
}

