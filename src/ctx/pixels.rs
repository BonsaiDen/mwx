// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::handle::Pixel;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Pixels {
    pub(crate) inner: pixels::Pixels,
    pub(crate) mouse: Option<(i32, i32)>,
    pub(crate) width: u32,
    pub(crate) height: u32,
}

impl Pixels {
    pub fn mouse(&self) -> Option<(i32, i32)> {
        self.mouse
    }

    pub fn resize(&mut self, size: (u32, u32)) {
        let (width, height) = size;
        self.width = width;
        self.height = height;
        self.inner.resize_buffer(width, height);
    }

    pub fn fill(&mut self, color: &[u8; 4]) {
        for p in self.inner.get_frame().chunks_exact_mut(4) {
            p.copy_from_slice(color);
        }
    }

    pub fn size(&self) -> (u32, u32) {
        (self.width, self.height)
    }

    pub fn frame(&mut self) -> &mut [u8] {
        self.inner.get_frame()
    }

    pub fn with_pixels_mut<C: FnMut(i32, i32, Pixel), T: Iterator<Item=(i32, i32)>>(&mut self, pixels: T, mut callback: C) {
        for (x, y) in pixels {
            if x >= 0 && x < self.width as i32 && y >= 0 && y < self.height as i32 {
                let i = (x as usize + y as usize * self.width as usize) * 4;
                let frame = self.inner.get_frame();
                callback(x, y, Pixel(&mut frame[i..i + 4]))
            }
        }
    }

    pub fn with_pixel_mut<C: FnMut(Pixel)>(&mut self, x: i32, y: i32, mut callback: C) {
        if x >= 0 && x < self.width as i32 && y >= 0 && y < self.height as i32 {
            let i = (x as usize + y as usize * self.width as usize) * 4;
            let frame = self.inner.get_frame();
            callback(Pixel(&mut frame[i..i + 4]))
        }
    }

    pub fn pixel_mut(&mut self, x: i32, y: i32) -> Option<Pixel> {
        if x >= 0 && x < self.width as i32 && y >= 0 && y < self.height as i32 {
            let i = (x as usize + y as usize * self.width as usize) * 4;
            let frame = self.inner.get_frame();
            Some(Pixel(&mut frame[i..i + 4]))

        } else {
            None
        }
    }
}

