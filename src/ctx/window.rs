// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::time::Duration;
use std::collections::VecDeque;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use winit_input_helper::WinitInputHelper as Input;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{handle, Key, State};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Window<'a, T: 'static + State> {
    pub state: &'a mut T,
    pub input: &'a Input,
    pub(crate) dt: Duration,
    pub(crate) fps: f32,
    pub(crate) handle: &'a handle::Window,
    pub(crate) state_events: &'a mut VecDeque<T::Event>
}

impl<'a, T: State> Window<'a, T> {
    pub fn dt(&self) -> Duration {
        self.dt
    }

    pub fn fps(&self) -> f32 {
        self.fps
    }

    pub fn event(&mut self, e: T::Event) {
        self.state_events.push_back(e);
    }

    pub fn ctrl_shortcut_pressed(&self, key: Key) -> bool {
        self.input.held_control() && self.input.key_pressed(key)
    }

    pub fn set_title<S: Into<String>>(&mut self, title: S) {
        self.handle.set_title(title);
    }

    pub fn set_size(&mut self, size: (u32, u32)) {
        self.handle.set_size(size);
    }

    pub fn set_minimum_size(&mut self, size: (u32, u32)) {
        self.handle.set_minimum_size(size);
    }

    pub fn set_position(&mut self, position: (i32, i32)) {
        self.handle.set_position(position);
    }

    pub fn position(&self) -> (i32, i32) {
        self.handle.position()
    }

    pub fn size(&self) -> (u32, u32) {
        self.handle.size()
    }

    pub fn close(&mut self) {
        self.handle.close_mut();
    }
}

