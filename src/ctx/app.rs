// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;
use std::time::Duration;
use std::collections::{HashMap, VecDeque};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use winit::event_loop::EventLoopWindowTarget;


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{event, handle, AppInterface, State, WindowInterface};
use crate::window::{WindowBuilder, WindowMap, WindowInstance, WindowBuilderConfig};


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct App<'a, T: 'static + AppInterface> {
    dt: Duration,
    fps: f32,
    events: &'a mut VecDeque<event::App<T>>,
    state_events: &'a mut VecDeque<<T::State as State>::Event>,
    should_exit: &'a mut bool,
    windows: &'a HashMap<String, crate::config::Window>,
    window_instances: &'a mut WindowMap<T>,
    window_target: &'a EventLoopWindowTarget<()>,
}

impl<'a, T: 'static + AppInterface> App<'a, T> {
    pub fn dt(&self) -> Duration {
        self.dt
    }

    pub fn fps(&self) -> f32 {
        self.fps
    }

    pub fn window<'b>(&'b mut self) -> WindowBuilder<'b, 'a, T> {
        WindowBuilder::new(self)
    }

    pub fn event(&mut self, e: <T::State as State>::Event) {
        self.state_events.push_back(e);
    }

    pub fn exit(&mut self) {
        *self.should_exit = true;
    }
}

impl<'a, T: 'static + AppInterface> App<'a, T> {
    pub(crate) fn new(
        handle: &'a mut handle::App<T>,
        window_instances: &'a mut WindowMap<T>,
        window_target: &'a EventLoopWindowTarget<()>,

    ) -> Self {
        Self {
            dt: handle.dt,
            fps: handle.fps,
            events: &mut handle.events,
            state_events: &mut handle.state_events,
            should_exit: &mut handle.should_exit,
            windows: &handle.config.windows,
            window_instances,
            window_target
        }
    }

    pub(crate) fn pop_event(&mut self) -> Option<event::App<T>> {
        self.events.pop_front()
    }

    pub(crate) fn build_window<U: 'static + WindowInterface<T>>(
        &mut self,
        config: WindowBuilderConfig,
        inner: U

    ) -> Result<crate::handle::Window, Box<dyn Error>> {
        let window = WindowInstance::new(config, inner, self.windows, self.window_target)?;
        let handle = window.handle().clone();
        self.window_instances.insert(handle.id(), Some(window));
        Ok(handle)
    }
}

