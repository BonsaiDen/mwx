// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::texture::TextureData;


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Texture<'a, 'b> {
    pub(crate) ctx: &'a crate::ctx::UI<'b>,
    pub(crate) queued: bool,
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) data: &'a mut TextureData
}

impl<'a, 'b> Texture<'a, 'b> {
    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn bytes(&self) -> &[u8] {
        &self.data.bytes
    }

    pub fn bytes_mut(&mut self) -> &mut [u8] {
        &mut self.data.bytes
    }

    pub fn refresh(&mut self) {
        self.queued = true;
    }
}

impl<'a, 'b> Drop for Texture<'a, 'b> {
    fn drop(&mut self) {
        if self.queued {
            self.ctx.update_texture_data(self.data)
        }
    }
}

