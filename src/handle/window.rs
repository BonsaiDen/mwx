// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::cell::RefCell;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use winit::window::Window as WinitWindow;
use winit::dpi::LogicalSize;
use winit::dpi::PhysicalPosition;
use winit::window::{Icon, WindowId};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Window {
    id: WindowId,
    inner: Rc<RefCell<WindowInnerHandle>>
}

impl Window {
    pub fn id(&self) -> WindowId {
        self.id
    }

    pub fn size(&self) -> (u32, u32) {
        self.inner.borrow().size()
    }

    pub fn position(&self) -> (i32, i32) {
        self.inner.borrow().position()
    }

    pub fn set_title<S: Into<String>>(&self, title: S) {
        self.inner.borrow_mut().set_title(title);
    }

    pub fn set_size(&self, size: (u32, u32)) {
        self.inner.borrow_mut().set_size(size);
    }

    pub fn set_minimum_size(&self, size: (u32, u32)) {
        self.inner.borrow_mut().set_minimum_size(size);
    }

    pub fn set_position(&self, position: (i32, i32)) {
        self.inner.borrow_mut().set_position(position);
    }

    pub fn focus(&self) {
        self.inner.borrow_mut().focus()
    }

    pub fn set_icon(&self, icon: Option<Icon>) {
        self.inner.borrow_mut().set_icon(icon)
    }

    pub fn close(self) {
        self.close_mut()
    }

    pub(crate) fn new(id: WindowId, title: String) -> Self {
        Self {
            id,
            inner: Rc::new(RefCell::new(WindowInnerHandle {
                title,
                ..WindowInnerHandle::default()
            }))
        }
    }

    pub(crate) fn close_mut(&self) {
        self.inner.borrow_mut().should_close = true;
    }

    pub(crate) fn should_close(&self) -> bool {
        self.inner.borrow().should_close
    }

    pub(crate) fn initialize(&self) -> bool {
        if !self.inner.borrow().initialized {
            self.inner.borrow_mut().initialized = true;
            true

        } else {
            false
        }
    }

    pub(crate) fn clone(&self) -> Self {
        Self {
            id: self.id,
            inner: self.inner.clone()
        }
    }

    pub(crate) fn init_with_window(&self, window: &WinitWindow) {
        self.inner.borrow_mut().init_with_window(window);
    }

    pub(crate) fn apply_to_window(&self, window: &mut WinitWindow) {
        self.inner.borrow_mut().apply_to_window(window);
    }
}

#[derive(Default)]
struct WindowInnerHandle {
    initialized: bool,
    should_close: bool,
    title: String,
    position: (i32, i32),
    size: (u32, u32),
    new_title: Option<String>,
    new_position: Option<(i32, i32)>,
    new_size: Option<(u32, u32)>,
    new_minimum_size: Option<(u32, u32)>,
    should_focus: Option<()>,
    new_icon: Option<Option<Icon>>
}

impl WindowInnerHandle {
    fn size(&self) -> (u32, u32) {
        self.size
    }

    fn position(&self) -> (i32, i32) {
        self.position
    }

    fn set_title<S: Into<String>>(&mut self, title: S) {
        self.new_title = Some(title.into());
    }

    fn set_size(&mut self, size: (u32, u32)) {
        self.new_size = Some(size);
    }

    fn set_minimum_size(&mut self, size: (u32, u32)) {
        self.new_minimum_size = Some(size);
    }

    fn set_position(&mut self, position: (i32, i32)) {
        self.new_position = Some(position);
    }

    fn focus(&mut self) {
        self.should_focus = Some(());
    }

    fn set_icon(&mut self, icon: Option<Icon>) {
        self.new_icon = Some(icon);
    }

    fn init_with_window(&mut self, window: &WinitWindow) {
        let s = window.inner_size();
        self.size = (s.width, s.height);

        if let Ok(p) = window.outer_position() {
            self.position = (p.x, p.y);
        }
    }

    fn apply_to_window(&mut self, window: &mut WinitWindow) {
        if let Some(icon) = self.new_icon.take() {
            window.set_window_icon(icon);
        }
        if let Some(()) = self.should_focus.take() {
            window.focus_window();
        }
        if let Some(title) = self.new_title.take() {
            if title != self.title {
                window.set_title(&title);
                self.title = title;
            }
        }
        if let Some((x, y)) = self.new_position.take() {
            window.set_outer_position(PhysicalPosition::new(x, y));
        }
        if let Some((w, h)) = self.new_minimum_size.take() {
            window.set_min_inner_size(Some(LogicalSize::new(w, h)));
        }
        if let Some((w, h)) = self.new_size.take() {
            window.set_inner_size(LogicalSize::new(w, h));
        }
    }
}

