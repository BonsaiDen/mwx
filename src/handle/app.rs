// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::VecDeque;
use std::time::{Duration, Instant};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{config, event, AppInterface, State};


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct App<T: 'static + AppInterface> {
    pub config: crate::config::App<<T::State as State>::Config>,
    pub config_saved: crate::config::App<<T::State as State>::Config>,
    pub config_timer: Instant,
    pub fps: f32,
    pub dt: Duration,
    pub should_exit: bool,
    pub state_events: VecDeque<<T::State as State>::Event>,
    pub events: VecDeque<event::App<T>>,
}

impl<T: 'static + AppInterface> App<T> {
    pub fn from_app(app: &mut T) -> Self {
        let config = config::App::<<T::State as State>::Config>::load_from_toml().unwrap_or_else(|_| {
            config::App::<<T::State as State>::Config>::default()
        });
        *app.state_mut().config_mut() = config.inner.clone();
        Self {
            config: config.clone(),
            config_saved: config,
            config_timer: Instant::now(),
            fps: 0.0,
            dt: Duration::default(),
            should_exit: false,
            state_events: VecDeque::new(),
            events: VecDeque::new()
        }
    }

    pub fn compare_and_save_config(&mut self, app: &mut T, immediate: bool) {
        self.config.compare_and_save(
            &mut self.config_saved,
            app.state_mut().config_mut(),
            &mut self.config_timer,
            immediate
        );
    }
}

