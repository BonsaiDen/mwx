// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct Pixel<'a>(pub(crate) &'a mut [u8]);
impl<'a> Pixel<'a> {
    pub fn set(&mut self, rgba: &[u8; 4]) {
        self.0.copy_from_slice(rgba);
    }
}

