// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::rc::Rc;
use std::any::Any;
use std::error::Error;
use std::cell::RefCell;
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use winit::dpi::LogicalSize;
use winit::dpi::PhysicalPosition;
use winit::event_loop::EventLoopWindowTarget;
use winit_input_helper::WinitInputHelper as Input;
use winit::window::{Window as WinitWindow, WindowBuilder as WinitWindowBuilder, WindowId};
use imgui::{Context, SuspendedContext, FontConfig, FontSource};
use pixels::{PixelsBuilder, SurfaceTexture};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::renderer::Renderer;
use crate::{ctx, event, handle, AppInterface, ContextMenu, WindowInterface};


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub type WindowMap<T> = HashMap<WindowId, Option<WindowInstance<T>>>;
pub type ContextMenuCallback = Rc<RefCell<Option<Box<dyn Any>>>>;

type WindowEventCallback<U> = dyn Fn(&mut Box<dyn Any>, &mut ctx::Window<U>, event::Window);
type WindowCloseCallback<U> = dyn Fn(Box<dyn Any>, &mut ctx::Window<U>);
type WindowContextMenuCallbackMut<U, W, V> = dyn Fn(&mut ctx::Window<U>, &W, &ctx::UI) -> Option<V>;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct WindowBuilderConfig {
    title: String,
    size: (u32, u32, bool),
    resizeable: bool,
    clear_color: [f32; 4],
    pixel_size: Option<(u32, u32)>,
    priority: usize,
    used_icons: Option<imgui::FontGlyphRanges>
}

pub struct WindowBuilder<'a, 'b, T: 'static + AppInterface> {
    ctx: &'a mut ctx::App<'b, T>,
    title: String,
    size: (u32, u32, bool),
    resizeable: bool,
    clear_color: [f32; 4],
    pixels_size: Option<(u32, u32)>,
    priority: usize,
    used_icons: Option<&'a [&'static str]>
}

impl<'a, 'b, T: 'static + AppInterface> WindowBuilder<'a, 'b, T> {
    pub(crate) fn new(ctx: &'a mut ctx::App<'b, T>) -> Self {
        Self {
            ctx,
            title: "window".to_string(),
            size: (320, 240, true),
            resizeable: false,
            clear_color: [1.0, 0.0, 1.0, 1.0],
            pixels_size: None,
            priority: 0,
            used_icons: None
        }
    }

    pub fn title<S: Into<String>>(mut self, title: S) -> Self {
        self.title = title.into();
        self
    }

    pub fn minimum_size(mut self, minimum_size: (u32, u32)) -> Self {
        self.size = (minimum_size.0, minimum_size.1, true);
        self
    }

    pub fn resizeable(mut self, resizeable: bool) -> Self {
        self.resizeable = resizeable;
        self
    }

    pub fn clear_color(mut self, clear_color: [f32; 4]) -> Self {
        self.clear_color = clear_color;
        self
    }

    pub fn priority(mut self, priority: usize) -> Self {
        self.priority = priority;
        self
    }

    pub fn pixels_size(mut self, pixels_size: (u32, u32)) -> Self {
        self.pixels_size = Some(pixels_size);
        self
    }

    pub fn use_icons(mut self, icons: &'a [&'static str]) -> Self {
        self.used_icons = Some(icons);
        self
    }

    pub fn build<U: 'static + WindowInterface<T>>(self, inner: U) -> Result<handle::Window, Box<dyn Error>> {
        self.ctx.build_window(WindowBuilderConfig {
            title: self.title,
            size: self.size,
            resizeable: self.resizeable,
            clear_color: self.clear_color,
            pixel_size: self.pixels_size,
            priority: self.priority,
            used_icons: self.used_icons.map(|icons| {
                nerdfont::font_glyph_ranges(icons)
            })

        }, inner)
    }
}


// Implementation -------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct WindowInstance<T: 'static + AppInterface> {
    start: std::time::Instant,
    inner: Box<dyn Any>,
    handle: handle::Window,
    input: Input,
    window: WinitWindow,
    pixels_context: ctx::Pixels,
    imgui_suspended_context: Option<SuspendedContext>,
    renderer: Rc<RefCell<Renderer>>,
    active_context_menu: ContextMenuCallback,
    next_context_menu: ContextMenuCallback,
    context_action: Option<<T::ContextMenu as ContextMenu<T::State>>::Action>,
    config: WindowBuilderConfig,
    clear_color: wgpu::Color,
    cursor_inside: bool,
    initial_draw: bool,
    event: Box<WindowEventCallback<T::State>>,
    context_menu: Box<WindowContextMenuCallbackMut<
        T::State,
        T::ContextMenu,
        <T::ContextMenu as ContextMenu<T::State>>::Action
    >>,
    close: Box<WindowCloseCallback<T::State>>,
}

impl<T: 'static + AppInterface> WindowInstance<T> {
    pub fn new<U: 'static + WindowInterface<T>>(
        mut config: WindowBuilderConfig,
        inner: U,
        window_configs: &HashMap<String, crate::config::Window>,
        window_target: &EventLoopWindowTarget<()>

    ) -> Result<Self, Box<dyn Error>> {
        let start = std::time::Instant::now();
        let (width, height, is_minimum) = config.size;
        let mut stored_size = None;
        let mut stored_position: Option<(i32, i32)> = None;
        if let Some(config) = window_configs.get(&config.title) {
            stored_size = config.size;
            stored_position = config.position;
        }
        let window = {
            let inner_size = LogicalSize::new(width, height);
            let stored_size = if let Some((width, height)) = stored_size {
                LogicalSize::new(width, height)

            } else {
                inner_size
            };
            let mut builder = WinitWindowBuilder::new()
                .with_title(&config.title)
                .with_inner_size(stored_size);

            if is_minimum {
                builder = builder.with_min_inner_size(inner_size);
            }
            if let Some(position) = stored_position {
                 builder = builder.with_position(PhysicalPosition::new(position.0, position.1))
            }
            builder.with_resizable(config.resizeable).with_visible(false).build(window_target)?
        };

        let mut imgui = {
            let mut imgui = Context::create();
            imgui.set_ini_filename(None);

            // Configure Fonts
            let hidpi_factor = window.scale_factor().round();
            let font_size = (13.0 * hidpi_factor) as f32 * 2.0;
            imgui.io_mut().font_global_scale = (1.0 / hidpi_factor) as f32 / 2.0;

            if let Some(glyph_ranges) = config.used_icons.take() {
                imgui.fonts().add_font(&[FontSource::TtfData {
                    data: nerdfont::font_data(),
                    size_pixels: font_size,
                    config: Some(FontConfig {
                        oversample_h: 1,
                        oversample_v: 3,
                        pixel_snap_h: false,
                        size_pixels: font_size,
                        rasterizer_multiply: 1.5,
                        glyph_ranges,
                        ..Default::default()
                    })
                }]);

            } else {
                imgui.fonts().add_font(&[FontSource::DefaultFontData {
                    config: Some(FontConfig {
                        oversample_h: 1,
                        oversample_v: 1,
                        pixel_snap_h: false,
                        size_pixels: font_size,
                        ..Default::default()
                    })
                }]);
            }

            // Fix incorrect alpha values for sRGB framebuffers
            let style = imgui.style_mut();
            for color in 0..style.colors.len() {
                style.colors[color][3] = rgba_gamma(style.colors[color])[3];
            }

            // Fix default table colors
            let c = style.colors[imgui::StyleColor::TableRowBg as usize];
            style.colors[imgui::StyleColor::TableRowBgAlt as usize] = c;

            // Adjust scroll bar colors
            let c = style.colors[imgui::StyleColor::Header as usize];
            style.colors[imgui::StyleColor::ScrollbarGrab as usize] = c;
            let c = style.colors[imgui::StyleColor::HeaderActive as usize];
            style.colors[imgui::StyleColor::ScrollbarGrabActive as usize] = c;
            let c = style.colors[imgui::StyleColor::HeaderHovered as usize];
            style.colors[imgui::StyleColor::ScrollbarGrabHovered as usize] = c;
            imgui
        };

        let (pixels_context, clear_color) = {
            let (width, height) = config.pixel_size.unwrap_or((32, 32));
            let clear_color = rgba_gamma(config.clear_color);
            let clear_color = wgpu::Color {
                r: clear_color[0] as f64,
                g: clear_color[1] as f64,
                b: clear_color[2] as f64,
                a: clear_color[3] as f64,
            };
            let window_size = window.inner_size();
            let surface_texture = SurfaceTexture::new(window_size.width, window_size.height, &window);
            let inner = PixelsBuilder::new(width, height, surface_texture)
                // TODO switch to AutoNoVsync after upgrade to wgpu 0.13
                .present_mode(wgpu::PresentMode::Immediate)
                .clear_color(clear_color)
                .request_adapter_options(wgpu::RequestAdapterOptions {
                    power_preference: wgpu::PowerPreference::HighPerformance,
                    force_fallback_adapter: false,
                    compatible_surface: None,

                }).build()?;

            (ctx::Pixels {
                inner,
                mouse: None,
                width,
                height

            }, clear_color)
        };
        log::info!("Initialized in {:?}", start.elapsed());

        let mut renderer = Renderer::new(&mut imgui, &pixels_context);
        renderer.platform.attach_window(
            imgui.io_mut(),
            &window,
            imgui_winit_support::HiDpiMode::Default,
        );
        log::info!("Renderable after {:?}", start.elapsed());

        Ok(Self {
            start,
            inner: Box::new(inner),
            input: Input::new(),
            handle: handle::Window::new(window.id(), config.title.clone()),
            window,
            pixels_context,
            imgui_suspended_context: Some(imgui.suspend()),
            renderer: Rc::new(RefCell::new(renderer)),
            active_context_menu: Rc::new(RefCell::new(None)),
            next_context_menu: Rc::new(RefCell::new(None)),
            context_action: None,
            cursor_inside: true,
            initial_draw: true,
            clear_color,
            config,
            event: Box::new(|inner, ctx, event| {
                let inner = inner.downcast_mut::<U>().expect("window close downcast failed");
                inner.event(ctx, event);
            }),
            context_menu: Box::new(|ctx, menu, ui| {
                menu.render_ui(ctx, ui)
            }),
            close: Box::new(|inner, ctx| {
                let inner = inner.downcast::<U>().expect("window close downcast failed");
                inner.closed(ctx);
            }),
        })
    }

    pub fn priority(&self) -> usize {
        self.config.priority
    }

    pub fn handle(&self) -> &handle::Window {
        &self.handle
    }

    pub fn name(&self) -> &str {
        &self.config.title
    }

    pub fn position(&self) -> (i32, i32) {
        self.window.outer_position().map(|p| (p.x, p.y)).unwrap_or((0, 0))
    }

    pub fn action(&mut self) -> Option<<T::ContextMenu as ContextMenu<T::State>>::Action> {
        self.context_action.take()
    }

    pub fn size(&self) -> (u32, u32) {
        let size = self.window.inner_size();
        (size.width, size.height)
    }

    pub fn window_event(&mut self, state: &mut T::State, app: &mut handle::App<T>, event: &winit::event::Event<()>) {
        let mut imgui = self.imgui_suspended_context.take().unwrap().activate().expect("failed to resume imgui");
        if let winit::event::Event::WindowEvent { event: winit::event::WindowEvent::CursorEntered { .. }, .. } = event {
            self.cursor_inside = true;

        } else if let winit::event::Event::WindowEvent { event: winit::event::WindowEvent::CursorLeft { .. }, .. } = event {
            self.cursor_inside = false;
            imgui.io_mut().mouse_pos = [f32::MAX, f32::MAX];

        } else if let winit::event::Event::WindowEvent { event: winit::event::WindowEvent::DroppedFile(path), .. } = event {
            let mut ctx = ctx::Window {
                state,
                input: &self.input,
                dt: app.dt,
                fps: app.fps,
                handle: &self.handle,
                state_events: &mut app.state_events
            };
            (self.event)(&mut self.inner, &mut ctx, event::Window::DroppedFile(path.to_path_buf()));
        }
        self.renderer.borrow_mut().platform.handle_event(imgui.io_mut(), &self.window, event);
        self.imgui_suspended_context = Some(imgui.suspend());
        self.input.update(event);
    }

    pub fn event(&mut self, event: &winit::event::Event<()>) {
        if let winit::event::Event::DeviceEvent { event: winit::event::DeviceEvent::Button { .. }, .. } = event {
            if !self.cursor_inside {
                *self.active_context_menu.borrow_mut() = None;
            }
        }
        self.input.update(event);
    }

    pub fn init(&mut self, state: &mut T::State, app: &mut handle::App<T>) {
        let mut ctx = ctx::Window {
            state,
            input: &self.input,
            dt: app.dt,
            fps: app.fps,
            handle: &self.handle,
            state_events: &mut app.state_events
        };
        self.handle.init_with_window(&self.window);
        (self.event)(&mut self.inner, &mut ctx, event::Window::Init);
    }

    pub fn update(&mut self, state: &mut T::State, app: &mut handle::App<T>) {
        let mut ctx = ctx::Window {
            state,
            input: &self.input,
            dt: app.dt,
            fps: app.fps,
            handle: &self.handle,
            state_events: &mut app.state_events
        };
        self.handle.init_with_window(&self.window);
        (self.event)(&mut self.inner, &mut ctx, event::Window::Tick);
        self.window.request_redraw();
    }

    pub fn redraw(&mut self, state: &mut T::State, app: &mut handle::App<T>) {
        // Make sure to apply window resize in sync with pixel buffer resize to avoid
        // wgpu validation crashes
        self.handle.apply_to_window(&mut self.window);

        // Keep pixels surface in sync with window dimensions
        if let Some(size) = self.input.window_resized() {
            self.pixels_context.inner.resize_surface(size.width, size.height);
        }

        self.handle.init_with_window(&self.window);
        let mut ctx = ctx::Window {
            state,
            input: &self.input,
            dt: app.dt,
            fps: app.fps,
            handle: &self.handle,
            state_events: &mut app.state_events
        };

        // Draw PixelContext
        let mut pixels_screen_layout = (0.0, 0.0, 1.0);
        if self.config.pixel_size.is_some() {
            let (pixel_scale, rect) = {
                let context = self.pixels_context.inner.context();
                let scale_factor = self.window.scale_factor();
                let physical_size = self.window.inner_size().to_logical::<f32>(scale_factor);
                let pixel_size = (context.texture_extent.width as f32, context.texture_extent.height as f32);
                (
                    (physical_size.width / pixel_size.0).min(physical_size.height / pixel_size.1).max(1.0).floor(),
                    context.scaling_renderer.clip_rect()
                )
            };
            pixels_screen_layout = (rect.0 as f32, rect.1 as f32, pixel_scale);

            // Calculate mouse position relative to pixel buffer
            self.pixels_context.mouse = if let Some((x, y)) = self.input.mouse() {
                let s = self.window.scale_factor();
                let cursor_position: winit::dpi::LogicalPosition<f32> = winit::dpi::LogicalPosition::new(x, y);
                let cursor_position = cursor_position.to_physical::<f32>(s).into();
                self.pixels_context.inner.window_pos_to_pixel(cursor_position).ok().map(|(x, y)| (x as i32, y as i32))

            } else {
                None
            };

            // Render
            (self.event)(&mut self.inner, &mut ctx, event::Window::RenderPixels(&mut self.pixels_context));
        }

        // Re-activate imgui context and draw UI
        let mut imgui = self.imgui_suspended_context.take().unwrap().activate().expect("failed to resume imgui");
        let ui_frame = {
            let io = imgui.io_mut();
            io.update_delta_time(ctx.dt);
            self.renderer.borrow_mut().platform.prepare_frame(io, &self.window).ok();

            // Window UI
            let ui_frame = imgui.frame();
            let ui = ctx::UI {
                imgui: &ui_frame,
                pixels: &mut self.pixels_context,
                renderer: self.renderer.clone(),
                pixels_screen_layout,
                context_menu: self.next_context_menu.clone(),
                has_context_menu: self.active_context_menu.borrow().is_some()
            };
            (self.event)(&mut self.inner, &mut ctx, event::Window::RenderUI(&ui));

            // Context Menu
            if let Some(m) = self.next_context_menu.borrow_mut().take() {
                *self.active_context_menu.borrow_mut() = Some(m);
                ui_frame.open_popup("WINDOW_CONTEXT_MENU");
            }
            let mut open = false;
            if let Some(menu) = self.active_context_menu.borrow_mut().as_ref() {
                if let Some(menu) = menu.downcast_ref::<T::ContextMenu>() {
                    ui_frame.popup("WINDOW_CONTEXT_MENU", || {
                        self.context_action = (self.context_menu)(&mut ctx, menu, &ui);
                        open = true;
                    });
                }
            }
            if !open {
                *self.active_context_menu.borrow_mut() = None;
            }
            ui_frame
        };

        // Render to Window
        self.pixels_context.inner.render_with(|encoder, render_target, context| {
            let load = if self.config.pixel_size.is_some() {
                context.scaling_renderer.render(encoder, render_target);
                wgpu::LoadOp::Load

            } else {
                wgpu::LoadOp::Clear(self.clear_color)
            };
            self.renderer.borrow_mut().platform.prepare_render(&ui_frame, &self.window);
            self.renderer.borrow_mut().render(ui_frame.render(), encoder, render_target, context, load);
            Ok(())

        }).ok();

        // De-active imgui context
        self.imgui_suspended_context = Some(imgui.suspend());

        // Apply changes to underlying window
        self.handle.apply_to_window(&mut self.window);

        // Show window after first draw
        if self.initial_draw {
            log::info!("First draw after {:?}", self.start.elapsed());
            self.window.set_visible(true);
            self.initial_draw = false;
        }
    }

    pub fn close(self, state: &mut T::State, app: &mut handle::App<T>) {
        let mut ctx = ctx::Window {
            state,
            input: &self.input,
            dt: app.dt,
            fps: app.fps,
            handle: &self.handle,
            state_events: &mut app.state_events
        };
        self.handle.init_with_window(&self.window);
        (self.close)(self.inner, &mut ctx);
        app.events.push_back(event::App::WindowClosed(self.handle.id()));
    }
}


// Helpers --------------------------------------------------------------------
// ----------------------------------------------------------------------------
fn rgba_gamma(color: [f32; 4]) -> [f32; 4] {
    const GAMMA: f32 = 2.2;
    let x = color[0].powf(GAMMA);
    let y = color[1].powf(GAMMA);
    let z = color[2].powf(GAMMA);
    let w = 1.0 - (1.0 - color[3]).powf(GAMMA);
    [x, y, z, w]
}

