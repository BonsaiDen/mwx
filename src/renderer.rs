// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use imgui::Context;
use imgui_winit_support::WinitPlatform;
use pixels::{PixelsContext, wgpu::TextureDimension};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::ctx;
use crate::texture::TextureData;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub(crate) struct Renderer {
    pub wgpu: imgui_wgpu::Renderer,
    pub platform: imgui_winit_support::WinitPlatform,
}

impl Renderer {
    pub fn new(imgui: &mut Context, pixels: &ctx::Pixels) -> Self {
        let platform = WinitPlatform::init(imgui);
        let device = pixels.inner.device();
        let queue = pixels.inner.queue();
        let mut config = imgui_wgpu::RendererConfig::new();
        config.texture_format = wgpu::TextureFormat::Bgra8UnormSrgb;
        config.sample_count = 1;
        Self {
            wgpu: imgui_wgpu::Renderer::new(imgui, device, queue, config),
            platform
        }
    }

    pub fn render(
        &mut self,
        data: &imgui::DrawData,
        encoder: &mut wgpu::CommandEncoder,
        render_target: &wgpu::TextureView,
        context: &PixelsContext,
        load: wgpu::LoadOp<wgpu::Color>
    ) {
        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: render_target,
                resolve_target: None,
                ops: wgpu::Operations {
                    load,
                    store: true,
                }
            }],
            depth_stencil_attachment: None,
        });
        self.wgpu.render(data, &context.queue, &context.device, &mut rpass).ok();
    }

    pub fn update_texture_data(&self, context: &PixelsContext, data: &TextureData) {
        if let Some(tex) = self.wgpu.textures.get(data.id) {
            tex.write(&context.queue, &data.bytes, tex.width(), tex.height());
        }
    }

    pub fn create_texture_data(&mut self, context: &PixelsContext, width: u32, height: u32, bytes: Vec<u8>) -> TextureData {
        let tex = imgui_wgpu::Texture::new(&context.device, &self.wgpu, imgui_wgpu::TextureConfig {
            size: wgpu::Extent3d {
                width: width as u32,
                height: height as u32,
                depth_or_array_layers: 1
            },
            label: None,
            format: None,
            usage: wgpu::TextureUsages::COPY_SRC | wgpu::TextureUsages::COPY_DST | wgpu::TextureUsages::TEXTURE_BINDING,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            sampler_desc: wgpu::SamplerDescriptor {
                label: Some("nearest neighbor sampler"),
                address_mode_u: wgpu::AddressMode::ClampToEdge,
                address_mode_v: wgpu::AddressMode::ClampToEdge,
                address_mode_w: wgpu::AddressMode::ClampToEdge,
                mag_filter: wgpu::FilterMode::Nearest,
                min_filter: wgpu::FilterMode::Nearest,
                mipmap_filter: wgpu::FilterMode::Nearest,
                lod_min_clamp: -100.0,
                lod_max_clamp: 100.0,
                compare: None,
                anisotropy_clamp: None,
                border_color: None,
            }
        });
        tex.write(&context.queue, &bytes, width as u32, height as u32);
        TextureData {
            id: self.wgpu.textures.insert(tex),
            bytes
        }
    }
}

