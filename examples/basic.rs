// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use mwx::{
    AppInterface,
    Config,
    ContextMenu,
    State,
    Texture,
    WindowInterface,
    FileDialog,
    imgui::Selectable,
    drw::{Color, PxlCanvasBuffer, Rotation},
    winit::event::VirtualKeyCode as Key,
    icon_str, icon_list
};
use nfde::FilterableDialogBuilder;
use serde::{Serialize, Deserialize};


// Window Implementations -----------------------------------------------------
// ----------------------------------------------------------------------------
struct BasicWindowPixels {
    r: f32
}

impl WindowInterface<BasicApp> for BasicWindowPixels {
    fn event(&mut self, window: &mut mwx::Window<BasicState>, event: mwx::event::Window) {
        match event {
            mwx::event::Window::Init => {},
            mwx::event::Window::Tick => {
                if window.input.key_pressed(Key::Tab) {
                    window.event(BasicEvent::OpenUiWindow);

                } else if window.input.key_released(Key::Escape) {
                    window.close();
                }
            },
            mwx::event::Window::RenderUI(ui) => {
                let (pos, size) = ui.pixel_offset_position([0.0, 0.0], [100.0, 25.0]);
                imgui::Window::new("PixelOverlayWindow")
                    .size(size, imgui::Condition::Always)
                    .position(pos, imgui::Condition::Always)
                    .title_bar(false)
                    .collapsible(false)
                    .resizable(false)
                    .build(ui, || {
                        ui.text("Overlay")
                    });

                if ui.is_mouse_clicked(imgui::MouseButton::Right) {
                    ui.open_context_menu(BasicContextMenu::Foo);
                }
            },
            mwx::event::Window::RenderPixels(pixels) => {
                let size = pixels.size();
                pixels
                    .frame()
                    .pxl_canvas(size)
                    .color(Color::BLACK)
                    .clear()
                    .color(Color::RED)
                    .push()
                        .translate((size.0 as f32 * 0.5, size.1 as f32 * 0.5))
                        .rotate(Rotation::Deg(self.r))
                        .rect((0.0, 0.0), (32.0, 32.0))
                        .pop();

                self.r += 1.0;
            },
            mwx::event::Window::DroppedFile(path) => {
                window.state.loaded_file = Some(path.to_string_lossy().to_string());
            }
        }
    }
}

struct BasicWindowUiX;

impl WindowInterface<BasicApp> for BasicWindowUiX {
    fn event(&mut self, window: &mut mwx::Window<BasicState>, event: mwx::event::Window) {
        match event {
            mwx::event::Window::Init => {},
            mwx::event::Window::Tick => {
                if window.input.key_released(Key::Escape) {
                    window.close();
                }
            },
            mwx::event::Window::RenderUI(ui) => {
                ui.button("Hover me!");
                if ui.is_mouse_hovering_rect(ui.item_rect_min(), ui.item_rect_max())  {
                    ui.text_tooltip("Hello");
                }
                ui.checkbox("Checked", &mut window.state.config.checkbox);
                if ui.is_mouse_clicked(imgui::MouseButton::Right) {
                    ui.open_context_menu(BasicContextMenu::Bar);
                }
                if let Some(file) = &window.state.loaded_file {
                    ui.text(format!("{} Loaded File: {}", mwx::icons::NF_OCT_FILE, file));

                } else {
                    ui.text(icon_str!(NF_OCT_FILE " Loaded File: -"));
                }
                if ui.button(icon_str!("Load File " NF_MDI_FILE_FIND)) {
                    if let Ok(Some(path)) = ui.open_file(|nfd| {
                        nfd.add_filter("GameBoy ROMs", "gb,gbc")

                    }) {
                        window.state.loaded_file = Some(path.to_string_lossy().to_string());
                    }
                }
            },
            mwx::event::Window::RenderPixels(_) => {},
            mwx::event::Window::DroppedFile(path) => {
                window.state.loaded_file = Some(path.to_string_lossy().to_string());
            }
        }
    }
}

struct BasicWindowUi {
    texture: Texture,
    r: f32
}

impl WindowInterface<BasicApp> for BasicWindowUi {
    fn event(&mut self, window: &mut mwx::Window<BasicState>, event: mwx::event::Window) {
        match event {
            mwx::event::Window::Init => {},
            mwx::event::Window::Tick => {
                if window.input.key_released(Key::Escape) {
                    window.close();
                }
            },
            mwx::event::Window::RenderUI(ui) => {
                let _c = ui.push_style_var(imgui::StyleVar::WindowBorderSize(0.0));
                let size = ui.io().display_size;
                imgui::Window::new("BasicWindowUi")
                    .size(size, imgui::Condition::Always)
                    .position([0.0, 0.0], imgui::Condition::Always)
                    .title_bar(false)
                    .collapsible(false)
                    .resizable(false)
                    .build(ui, || {
                        ui.text(format!("Counter: {}", window.state.value));
                        if ui.button("Reset Counter") {
                            window.state.value = 0;
                        }
                        {
                            let mut buffer = self.texture.handle(ui);
                            let size = (buffer.width(), buffer.height());
                            buffer.bytes_mut()
                                .pxl_canvas(size)
                                .color(Color::GREEN)
                                .clear()
                                .color(Color::WHITE)
                                .push()
                                    .translate((64.0, 64.0))
                                    .rotate(Rotation::Deg(self.r))
                                    .rect((0.0, 0.0), (32.0, 32.0))
                                    .pop();

                            buffer.refresh();
                        }
                        let list = ui.get_window_draw_list();
                        let p = ui.cursor_screen_pos();
                        list.add_image(self.texture.id(ui), [p[0], p[1]], [p[0] + 128.0, p[1] + 128.0]).build();
                        self.r -= 1.0;
                    });
                if ui.is_mouse_clicked(imgui::MouseButton::Right) {
                    ui.open_context_menu(BasicContextMenu::Foo);
                }
            },
            mwx::event::Window::RenderPixels(_) => {},
            mwx::event::Window::DroppedFile(path) => {
                window.state.loaded_file = Some(path.to_string_lossy().to_string());
            }
        }
    }
}


// App Types ------------------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
struct BasicState {
    value: u8,
    config: BasicConfig,
    loaded_file: Option<String>
}

impl State for BasicState {
    type Event = BasicEvent;
    type Config = BasicConfig;

    fn config_mut(&mut self) -> &mut Self::Config {
        &mut self.config
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct BasicConfig {
    #[serde(default)]
    ui_window_open: bool,
    #[serde(default)]
    checkbox: bool
}
impl Config for BasicConfig {}

enum BasicEvent {
    OpenUiWindow
}

enum BasicContextMenu {
    Foo,
    Bar
}

enum BasicAction {
    ResetCounter
}

impl ContextMenu<BasicState> for BasicContextMenu {
    type Action = BasicAction;

    fn render_ui(&self, window: &mut mwx::Window<BasicState>, ui: &mwx::UI) -> Option<BasicAction> {
        if Selectable::new(format!("Toggle Checkbox ({})", window.state.config.checkbox)).build(ui) {
            window.state.config.checkbox = !window.state.config.checkbox;
        }
        if Selectable::new(format!("Reset Counter ({})###reset_counter", window.state.value)).build(ui) {
            return Some(BasicAction::ResetCounter);
        }
        None
    }
}


// App Implementation ---------------------------------------------------------
// ----------------------------------------------------------------------------
#[derive(Default)]
struct BasicApp {
    pixel_handle: Option<mwx::handle::Window>,
    ui_handle: Option<mwx::handle::Window>,
    extra_handle: Option<mwx::handle::Window>,
    state: BasicState
}

impl BasicApp {
    fn open_ui_window(&mut self, app: &mut mwx::App<Self>) {
        self.state.config.ui_window_open = true;
        self.ui_handle = app.window()
            .title("imgui")
            .minimum_size((580, 600))
            .resizeable(false)
            .clear_color([0.1, 0.1, 0.1, 1.0])
            .priority(1)
            .build(BasicWindowUi {
                texture: Texture::new(128, 128, [0, 0, 0, 0]),
                r: 0.0

            }).ok();
    }
}

impl AppInterface for BasicApp {
    type Output = ();
    type State = BasicState;
    type ContextMenu = BasicContextMenu;

    fn state_mut(&mut self) -> &mut BasicState {
        &mut self.state
    }

    fn event(&mut self, app: &mut mwx::App<Self>, _audio: &mut mwx::Audio, event: mwx::event::App<Self>) {
        match event {
            mwx::event::App::Init => {
                if self.state.config.ui_window_open {
                    self.open_ui_window(app);
                }
                self.pixel_handle = app.window()
                    .title("pixels")
                    .minimum_size((320, 288))
                    .resizeable(true)
                    .clear_color([0.1, 0.1, 0.1, 1.0])
                    .pixels_size((160, 144))
                    .priority(0)
                    .build(BasicWindowPixels {
                        r: 0.0
                    }).ok();

                self.extra_handle = app.window()
                    .title("imguix")
                    .use_icons(icon_list![
                        NF_OCT_FILE,
                        NF_MDI_FILE_FIND
                    ])
                    .build(BasicWindowUiX).ok();
            },
            mwx::event::App::Tick => {
                self.state.value = self.state.value.wrapping_add(1);
            },
            mwx::event::App::Action(BasicAction::ResetCounter) => {
                self.state.value = 0;
            },
            mwx::event::App::WindowClosed(id) => {
                if self.pixel_handle.as_ref().map(|h| h.id()) == Some(id) {
                    app.exit();

                } else if self.ui_handle.as_ref().map(|h| h.id()) == Some(id) {
                    self.state.config.ui_window_open = false;
                    self.ui_handle.take();
                }
            },
            mwx::event::App::State(BasicEvent::OpenUiWindow) => {
                if self.ui_handle.is_none() {
                    self.open_ui_window(app);
                }
            }
        }
    }

    fn closed(self, _app: &mut mwx::App<Self>) -> mwx::Output<()> {
        Ok(())
    }
}


// Main -----------------------------------------------------------------------
// ----------------------------------------------------------------------------
fn main() {
    let app = BasicApp::default();
    app.run(Some(60)).expect("running example failed");
}

